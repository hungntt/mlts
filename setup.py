from setuptools import find_packages, setup

setup(
        name='src',
        packages=find_packages(),
        version='0.1.0',
        description='template repository for data science projects based on https://drivendata.github.io/cookiecutter-data-science/',
        author='Abdullah, Hung Nguyen, Yongxu Ren',
        license='MIT',
        install_requires=[
            'click',
            'pathlib',
            'setuptools',
            'pandas',
            'numpy',
            'lightgbm',
            'pandas_profiling',
            'autoviz',
            'dataprep',
            'seaborn',
            'joblib',
            'sklearn',
            'scikit-learn',
            'tqdm',
        ]
)
