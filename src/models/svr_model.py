from sklearn.svm import SVR
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn import metrics
from src.features.build_features import build_features
from src.visualization import visualize
import joblib
from tqdm import tqdm
import numpy as np
import pandas as pd


def resampling(df: pd.DataFrame) -> list[pd.DataFrame]:
    """
    Separate input dataframe to different crypto coins, and resample the data, in order to reduce the number of samples. 
    So that SVR model would not cost too much Memory and regression converge much faster.  
    Parameters
    ----------
    df: dataframe.

    Returns
    -------
    df_list: list[pd.DataFrame]
        a list of dataframes, each contain 
    """
    id_list = df['Asset_ID'].unique()
    id_list.sort()

    df_list = []

    for id in id_list:
        data = df[df['Asset_ID'].isin([id])]

        # reample the data, get target in each day
        data['timestamp'] = pd.to_datetime(data['timestamp'], unit='s')
        data = data.set_index('timestamp')

        data_early = data['2018':'2021']
        data_early = data_early.sample(n=500)

        data_late = data['2021']
        data_late = data_late.sample(n=0)

        data = pd.concat([data_early, data_late])
        print(data)

        data.dropna(inplace=True)

        df_list.append(data)

    return df_list


def train_std(df_list: list[pd.DataFrame]) -> list[pd.DataFrame]:
    """
    Standardize the training data, in order to speed up the converge of SVR regressor.
    Save series of standardization models to local folder.
    
    Parameters
    ----------
    df_list: list[pd.DataFrame]
        a list of dataframes, each contain samples of one kind of crypto coin
    
    Returns
    -------
    df_list: list[pd.DataFrame] 
        a list of dataframe, standardized training data 
    """
    scalers = {}
    count = 0
    cols = ['Count', 'Open', 'High', 'Low', 'Close', 'Volume']

    for df in df_list:
        scaler = StandardScaler()
        df[cols] = scaler.fit_transform(df[cols])
        # joblib.dump(scaler, 'scaler%s.pkl' %count)
        scalers[count] = scaler
        count += 1

    return df_list, scalers


def test_std(df: pd.DataFrame, scalers: StandardScaler) -> pd.DataFrame:
    """
    Load those saved standardization models to standardize the testing data.
    Insure the train and test data could be standardized by same mean and standard values
    
    Parameters
    ----------
    df: pd.DataFrame
        a list of dataframes, each contain samples of one kind of crypto coin
    
    Returns
    -------
    df: pd.DataFrame 
        a list of dataframe, standardized training data 
    """
    for index, row in df.iterrows():
        # load model
        num = int(row['Asset_ID'])
        # test_scalar = joblib.load('scaler%s.pkl' %num)
        test_scalar = scalers[num]
        df.loc[index, 'Count':'Volume'] = test_scalar.transform(
            df.loc[index, 'Count':'Volume'].values.reshape(1, 6)
        )[0]

    return df


def svr_fit(df_list: list[pd.DataFrame]) -> dict[GridSearchCV]:
    """
    Train the model for each data frame in df_list. 
    Use SVR as regression model and use Grid Search to look for best parameters.
    Use absolute error to evaluate the model.
    
    Parameters
    ----------
    df_list: list[pd.DataFrame]
        a list of dataframes, each contain samples of one kind of crypto coin
    
    Returns
    -------
    models: dict[GridSearchCV] 
        models for 14 different coins
    """
    models = {}

    count = 0

    print("Train SVR Model")
    for df in tqdm(df_list):
        x_train = df.loc[:, 'Count':'Volume']
        y_train = df.loc[:, 'Target']

        parameters = {
            'kernel': ['rbf'],
            'gamma': [0.1, 0.01, 0.001],
            'C': np.logspace(0, 5, num=6, base=2.0),
            'epsilon': [0.0001],
            'cache_size': [1000]
        }

        svr = SVR()
        grid_search = GridSearchCV(
            svr,
            parameters,
            cv=10,
            n_jobs=4,
            scoring='neg_mean_absolute_error',
            return_train_score=True
        )

        grid_search.fit(x_train, y_train)

        # store the model as .pkl file
        # joblib.dump(grid_search, 'svr%s.pkl' %count)
        models[count] = grid_search
        count += 1

        print(
            'Mean Train score: {}'.format(
                grid_search.cv_results_['mean_test_score']
            )
        )

    return models


def svr_pred(df: pd.DataFrame, models: dict[GridSearchCV]) -> pd.DataFrame:
    """
    Predict the "Target" value with the input future dataframe.
    Load the saved svr models from local folder.
    
    Parameters
    ----------
    df: pd.Dataframe
        each dataframe contain features of one future time point
    
    Returns
    -------
    df: pd.Dataframe 
        return a dataframe with a prediction value
    """
    pred_df = pd.DataFrame(columns=['Target'])
    for index, row in df.iterrows():
        # load model
        num = int(row['Asset_ID'])
        # test_model = joblib.load('svr%s.pkl' %num)
        test_model = models[num]

        x_test = df.loc[index, 'Count':'Volume'].values.reshape(1, 6)
        y_pred = test_model.predict(x_test)[0]
        pred_df = pred_df.append({'Target': y_pred}, ignore_index=True)

    return pred_df


def train_svr(df: pd.DataFrame) -> dict[GridSearchCV]:
    """
    train pipeline of svr model
    
    Parameters
    ----------
    df: pd.Dataframe
        dataframe contain from input file
    
    Returns
    -------
    models: dict[GridSearchCV] 
        trained models
    """
    df_list = resampling(df)
    df_list, scalers = train_std(df_list)
    models = svr_fit(df_list)
    return models, scalers


def test_svr(
    df_list: list[pd.DataFrame], id_list: list[float],
    models: dict[GridSearchCV], scalers: dict[StandardScaler]
):
    """
    Test models and draw a graph to compare them
    
    Parameters
    ----------
    df_list: list[pd.DataFrame]
        a list of dataframes of each coins at same time point
    id_list: list[float] 
        a list of timestamp
    models: dict[GridSearchCV]
        a dict contain all trained models
    """
    y_test_list = []
    y_pred_list = []

    print("==========Test SVR Model==========")
    for df in tqdm(df_list):
        test_std(df, scalers)

        y_test = df.loc[:, 'Target']
        y_test_list.append(y_test.values[0])
        # print("y test = ", y_test)

        y_pred = svr_pred(df, models).loc[:, 'Target']
        y_pred_list.append(y_pred.values[0])

    visualize.pred_test_compare(id_list, y_pred_list, y_test_list)

    print("MAE = ", metrics.mean_absolute_error(y_pred_list, y_test_list))
    print("MSE = ", metrics.mean_squared_error(y_pred_list, y_test_list))
    print(
        "MAPE = ",
        metrics.mean_absolute_percentage_error(y_pred_list, y_test_list)
    )


if __name__ == '__main__':
    df_train = pd.read_csv('../data/train.csv')
    df_asset = pd.read_csv('../data/asset_details.csv')
    df_test = pd.read_csv('../data/supplemental_train.csv')

    df_ReadyForFit = build_features(df_train, df_asset)

    print('===== Training SVR Model =====')
    models, scalers = train_svr(df_ReadyForFit)

    print('===== Testing SVR Model =====')

    df_test = df_test.head(10000)
    timestamp_list = df_test['timestamp'].unique()
    timestamp_list.sort()
    pd.set_option('mode.chained_assignment', None)

    test_df_list = []

    for id in timestamp_list:
        data = df_test[df_test['timestamp'].isin([id])]
        data.reset_index(drop=True, inplace=True)
        test_df_list.append(data)

    test_svr(test_df_list, timestamp_list, models, scalers)
