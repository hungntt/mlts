import warnings

import numpy as np
import pandas as pd

warnings.filterwarnings('ignore')


def feature_adjust(df, df_asset):
    """
    adjust features with merging and dropping features.

    Parameters
    ----------
    df: input dataframe.

    Returns
    -------
    x: pd.DataFrame
        the dataframe contains adjusted features.
    """
    feature_cols = [
        'Asset_ID', 'Close', 'Count', 'High', 'Low', 'Open', 'VWAP', 'Volume', 
        'timestamp'
    ]
    categories = ["Asset_ID", "timestamp"]  # Bull_Bear

    x = df[feature_cols].copy()
    df = pd.merge(df, df_asset, on="Asset_ID", how="left")
    df = df.drop("Asset_Name", axis=1)
    return x


# reduce memory usage
def reduce_mem_usage(df):
    """
    To reduce the memory usage of dataset.
    Parameters
    ----------
    df: dataframe.

    Returns
    -------
    df: pd.DataFrame
        reduced memory dataframe where redundant data types are replaced with proper types.
    """
    start_mem = df.memory_usage().sum() / 1024**2
    print('Memory usage of dataframe is {:.2f} MB'.format(start_mem))

    for col in df.columns:
        col_type = df[col].dtype

        if col_type != object:
            c_min = df[col].min()
            c_max = df[col].max()
            if str(col_type)[:3] == 'int':
                if c_min > np.iinfo(np.int8
                                    ).min and c_max < np.iinfo(np.int8).max:
                    df[col] = df[col].astype(np.int8)
                elif c_min > np.iinfo(np.int16).min and c_max < np.iinfo(
                    np.int16
                ).max:
                    df[col] = df[col].astype(np.int16)
                elif c_min > np.iinfo(np.int32).min and c_max < np.iinfo(
                    np.int32
                ).max:
                    df[col] = df[col].astype(np.int32)
                elif c_min > np.iinfo(np.int64).min and c_max < np.iinfo(
                    np.int64
                ).max:
                    df[col] = df[col].astype(np.int64)
            else:
                if c_min > np.finfo(np.float16).min and c_max < np.finfo(
                    np.float16
                ).max:
                    df[col] = df[col].astype(np.float16)
                elif c_min > np.finfo(np.float32).min and c_max < np.finfo(
                    np.float32
                ).max:
                    df[col] = df[col].astype(np.float32)
                else:
                    df[col] = df[col].astype(np.float64)
    #         else:
    #             df[col] = df[col].astype('category')

    df["Count"] = df["Count"].astype(np.int16)

    end_mem = df.memory_usage().sum() / 1024**2

    print('Memory usage after optimization is: {:.2f} MB'.format(end_mem))
    print(
        'Decreased by {:.1f}%'.format(100 * (start_mem - end_mem) / start_mem)
    )

    return df


def build_features(df_train, df_asset):
    df_train["VWAP"] = df_train["VWAP"].replace([np.inf, -np.inf], np.nan)
    vwap_mean = df_train[["Asset_ID",
                          "VWAP"]].groupby("Asset_ID").mean().reset_index()
    vwap_mean.columns = ["Asset_ID", "vwap_mean"]

    df_train = pd.merge(df_train, vwap_mean, on="Asset_ID", how="left")
    df_train.loc[(df_train["VWAP"].isnull()), "VWAP"] = df_train["vwap_mean"]
    df_train = df_train.drop("vwap_mean", axis=1)

    df_train = df_train[~df_train.isin([np.nan, np.inf, -np.inf]).
                        any(1)].reset_index(drop=True)

    # add weight of each coin into the dataframe
    # df_train = feature_adjust(df_train, df_asset)

    df_train = reduce_mem_usage(df_train)

    return df_train
