import matplotlib.pyplot as plt


def pred_test_compare(id_list, pred_list, test_list):
    """
    draw a graph of the comparison between predict values and test values
        id_list: a list of timestamp
        pred_list: a list of predict values
        test_list: a list of test values
    """
    plt.xlabel("time")
    plt.ylabel("Target")

    plt.plot(id_list, test_list, label='Target')
    plt.plot(id_list, pred_list, label='Prediction')

    plt.legend()
    plt.show()
    plt.savefig("../visualization/test.svg", format="svg")
