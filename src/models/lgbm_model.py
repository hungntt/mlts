import warnings

import lightgbm as lgb
import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn import metrics
from src.visualization import visualize

warnings.filterwarnings('ignore')

# Initialize
lgb_params = {
    "objective": "regression",
    "n_estimators": 5000,
    "num_leaves": 300,
    "learning_rate": 0.09,
    "random_seed": 1234
}

feature_cols = [
    'Asset_ID', 'Close', 'Count', 'High', 'Low', 'Open', 'VWAP', 'Volume',
    'timestamp'
]  # "Weight"
categories = ["Asset_ID", "timestamp"]


def feature_adjust(df: pd.DataFrame) -> pd.DataFrame:
    """
    adjust features with merging and dropping features.

    Parameters
    ----------
    df: input dataframe.

    Returns
    -------
    x: pd.DataFrame
        the dataframe contains adjusted features.
    """
    df_feature_cols = [
        'Asset_ID', 'Close', 'Count', 'High', 'Low', 'Open', 'VWAP', 'Volume',
        'timestamp'
    ]

    x = df[df_feature_cols].copy()
    df = pd.merge(df, df_asset, on="Asset_ID", how="left")
    df = df.drop("Asset_Name", axis=1)
    return x


def get_model(asset_id):
    """
    create a simple LGBMRegressor which fits on the train and test set.
    Parameters
    ----------
    asset_id: the Asset ID of cryptocurrencies.

    Returns
    -------
    model: lgb.LGBMRegressor
        a simple LGBMRegressor.
    """
    df = df_train[df_train["Asset_ID"] == asset_id]

    x = feature_adjust(df)
    y = df['Target'].copy()

    model = lgb.LGBMRegressor(**lgb_params)
    model.fit(x, y)

    return model


def reduce_mem_usage(df: pd.DataFrame) -> pd.DataFrame:
    """
    To reduce the memory usage of dataset.
    Parameters
    ----------
    df: dataframe.

    Returns
    -------
    df: pd.DataFrame
        reduced memory dataframe where redundant data types are replaced with proper types.
    """
    start_mem = df.memory_usage().sum() / 1024**2
    print('Memory usage of dataframe is {:.2f} MB'.format(start_mem))

    for col in df.columns:
        col_type = df[col].dtype

        if col_type != object:
            c_min = df[col].min()
            c_max = df[col].max()
            if str(col_type)[:3] == 'int':
                if c_min > np.iinfo(np.int8
                                    ).min and c_max < np.iinfo(np.int8).max:
                    df[col] = df[col].astype(np.int8)
                elif c_min > np.iinfo(np.int16).min and c_max < np.iinfo(
                    np.int16
                ).max:
                    df[col] = df[col].astype(np.int16)
                elif c_min > np.iinfo(np.int32).min and c_max < np.iinfo(
                    np.int32
                ).max:
                    df[col] = df[col].astype(np.int32)
                elif c_min > np.iinfo(np.int64).min and c_max < np.iinfo(
                    np.int64
                ).max:
                    df[col] = df[col].astype(np.int64)
            else:
                if c_min > np.finfo(np.float16).min and c_max < np.finfo(
                    np.float16
                ).max:
                    df[col] = df[col].astype(np.float16)
                elif c_min > np.finfo(np.float32).min and c_max < np.finfo(
                    np.float32
                ).max:
                    df[col] = df[col].astype(np.float32)
                else:
                    df[col] = df[col].astype(np.float64)

    df["Count"] = df["Count"].astype(np.int16)

    end_mem = df.memory_usage().sum() / 1024**2

    print('Memory usage after optimization is: {:.2f} MB'.format(end_mem))
    print(
        'Decreased by {:.1f}%'.format(100 * (start_mem - end_mem) / start_mem)
    )

    return df


if __name__ == "__main__":
    df_train = pd.read_csv('../data/train.csv')
    df_test = pd.read_csv('../data/supplemental_train.csv')
    df_asset = pd.read_csv('../data/asset_details.csv')

    df_train[df_train["Target"].isna()].head()
    df_train["VWAP"] = df_train["VWAP"].replace([np.inf, -np.inf], np.nan)

    vwap_mean = df_train[["Asset_ID",
                          "VWAP"]].groupby("Asset_ID").mean().reset_index()
    vwap_mean.columns = ["Asset_ID", "vwap_mean"]

    df_train = pd.merge(df_train, vwap_mean, on="Asset_ID", how="left")
    df_train.loc[(df_train["VWAP"].isnull()), "VWAP"] = df_train["vwap_mean"]
    df_train = df_train.drop("vwap_mean", axis=1)

    df_train = df_train[~df_train.isin([np.nan, np.inf, -np.inf]).
                        any(1)].reset_index(drop=True)

    df_train = reduce_mem_usage(df_train)
    df_test = reduce_mem_usage(df_test)

    asset_ids = list(df_asset["Asset_ID"].unique())
    asset_ids.sort()
    models = {}

    for asset_id in asset_ids:
        print(f"Training model for  ID={asset_id}")

        model = get_model(asset_id)
        models[asset_id] = model

    print('===== Testing SVR Model =====')
    df_test = df_test.head(10000)
    timestamp_list = df_test['timestamp'].unique()
    timestamp_list.sort()
    pd.set_option('mode.chained_assignment', None)

    test_df_list = []

    for id in timestamp_list:
        data = df_test[df_test['timestamp'].isin([id])]
        data.reset_index(drop=True, inplace=True)
        test_df_list.append(data)

    y_test_list = []
    y_pred_list = []

    for df in tqdm(test_df_list):
        y_test = df.loc[:, 'Target']
        y_test_list.append(y_test.values[0])
        for j, row in df.iterrows():
            model = models[row['Asset_ID']]
            row = pd.DataFrame(data=[row.values] * 1, columns=row.index)
            x_test = feature_adjust(row)
            y_pred = model.predict(x_test)[0]
            if row.at[0, 'Asset_ID'] == 0: y_pred_list.append(y_pred)

    visualize.pred_test_compare(timestamp_list, y_pred_list, y_test_list)

    print("MAE = ", metrics.mean_absolute_error(y_pred_list, y_test_list))
    print("MSE = ", metrics.mean_squared_error(y_pred_list, y_test_list))
    print(
        "MAPE = ",
        metrics.mean_absolute_percentage_error(y_pred_list, y_test_list)
    )
