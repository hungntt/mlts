# How to contribute

## GitLab Flow
- If not familiar please read this [intro to GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
Specifically the sections ***Merge/pull requests with GitLab flow***, ***Issue tracking with GitLab flow*** and ***Linking and closing issues from merge requests***
- For git commit messages, please use following shape: ```"[<task_type>] <short_description>"```, e.g.

## Coding style and formatting
- [PEP8](https://www.python.org/dev/peps/pep-0008/) guidelines are followed
- [PEP 484](https://www.python.org/dev/peps/pep-0484/) we use type hinting in addition to docstrings 
- we use [numpy style docstrings (with pep484)](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html)
- Use [yapf](https://github.com/google/yapf) for automatic style checking
- Run ```yapf --in-place --recursive src``` for code formatting (required by ci pipeline)
- 
## Write tests
- Unit tests are written for new features that are not covered by [existing tests](https://https://gitlab.cs.fau.de/mlts-project/mlts-project-template/-/tree/master/src/unittests)
