import os

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from tqdm import tqdm
from statsmodels.tsa.arima.model import ARIMA
from src.visualization import visualize
from sklearn import metrics

import warnings

warnings.filterwarnings("ignore")


# load data
def load_train_data(path: str) -> pd.DataFrame:
    """
    To load the train dataset.
    Parameters
    ----------
    path: path of loading train csv data.

    Returns
    -------
    pd.DataFrame
        the dataframe of training dataset after dropping duplicates.
    """
    print("Loading train data...")
    df_train = pd.read_csv(path, usecols=["timestamp", "Asset_ID", "Target"])
    # remove NAN/None/Dups
    df_train.dropna(axis=0, inplace=True)
    df_train.drop_duplicates(inplace=True)
    return df_train


def load_assets(path: str) -> pd.DataFrame:
    """
    To load the assets.

    Parameters
    ----------
    path: path of loading csv file.

    Returns
    -------
    pd.DataFrame
        the dataframe of the assets.
    """
    print("Loading asset details...")
    assets = pd.read_csv(path)
    return assets


def load_test_data(path: str) -> pd.DataFrame:
    """
    To load the test data.

    Parameters
    ----------
    path: the path to load the test set.
    -------
    the dataframe of the test set.
    """
    print("Loading test data...")
    df_test = pd.read_csv(path)
    return df_test


# visualisations
def plot_autocorrelation(df_train: pd.DataFrame, df_assets: pd.DataFrame):
    """
    To visualize the autocorrelation.

    Parameters
    ----------
    target: the target of train set.

    """
    print("Plotting Autocorrelation plots")
    fig = plt.figure(figsize=(8, 8))
    for index, row in df_assets.iterrows():
        target = df_train.loc[df_train["Asset_ID"] == row['Asset_ID']
                              ]["Target"].values
        if len(target) > 0:
            fig.add_subplot(4, 4, index + 1)
            plt.title(f"{row['Asset_Name']}")
            plt.xlabel("Lags")
            plt.acorr(target, maxlags=20)
            plt.grid(True)
    plt.savefig("../visualization/autocorr.svg")


def prepare_assets_models_dicts(
    df_train: pd.DataFrame, df_assets: pd.DataFrame
) -> dict:
    """
    Parameters
    ----------
    df_train: the train dataset.
    df_assets: the assets dataset.

    Returns
    -------
    asset_models: dict
        dictionary of assets over models.
    """
    asset_models = dict()
    for index, row in df_assets.iterrows():
        asset_models[row['Asset_ID']] = dict()
        asset_models[row['Asset_ID']]["name"] = row['Asset_Name']
        date = pd.to_datetime(
            df_train.loc[df_train["Asset_ID"] == row['Asset_ID']]["timestamp"],
            unit="s"
        ).values
        asset_models[row['Asset_ID']]["train_data"] = df_train.loc[
            df_train["Asset_ID"] == row['Asset_ID']]["Target"]
        asset_models[row['Asset_ID']]["train_data"].index = date
        asset_models[row['Asset_ID']]["train_data"].index = pd.DatetimeIndex(
            asset_models[row['Asset_ID']]["train_data"].index
        ).to_period("T")
    return asset_models


def train(df_train: pd.DataFrame, df_assets: pd.DataFrame):
    """
    train the ARIMA model trained over the dataset prepared from prepare_assets_models_dicts function.
    Parameters
    ----------
    df_train: the train dataset.
    df_assets: the assets dataset.

    Returns
    -------
    asset_models: the trained ARIMA model.
    """
    asset_models = prepare_assets_models_dicts(df_train, df_assets)
    for index, row in df_assets.iterrows():
        print(f"Starting training {row['Asset_Name']} - {row['Asset_ID']}")
        asset_models[row['Asset_ID']]["model"] = ARIMA(
            asset_models[row['Asset_ID']]["train_data"], order=(3, 1, 1)
        ).fit()
    return asset_models


def predict_test_data(df_test: pd.DataFrame, asset_models: dict):
    """
    predict the test data.

    Parameters
    ----------
    df_test: the test dataset.
    asset_models: models for predicting over assets.

    Returns
    -------
    target: the target derived from the test data.
    predicted: the prediction from asset_models.
    """
    target = list()
    predicted = list()
    for row in range(len(df_test.index)):
        try:
            inp = pd.to_datetime(df_test["timestamp"].iloc[row], unit="s")
            print(inp)
            ast_id = df_test["Asset_ID"].iloc[row]
            pred_df = asset_models[ast_id]["model"].predict(start=inp, end=inp)
            pred = pred_df.iloc[0]  # make your predictions here

        except Exception as e:
            print(e, asset_models[ast_id]["name"])
            pred = 0  # make your predictions here
        finally:
            target.append(df_test["Target"].iloc[row])
            predicted.append(pred)
    return target, predicted


def compute_pearson_corrcoef(x: np.ndarray, y: np.ndarray):
    """
    compute the pearson correlation.
    Parameters
    ----------
    x: the target data.
    y: the predicted data.

    Returns
    -------
    rho: np.ndarray
        the pearson correlation.
    """
    print("MAE = ", metrics.mean_absolute_error(y, x))
    print("MSE = ", metrics.mean_squared_error(y, x))
    print(
        "MAPE = ",
        metrics.mean_absolute_percentage_error(y, x)
    )
    rho = np.corrcoef(x, y)
    return rho


if __name__ == "__main__":
    train_path = 'src/data/train.csv'
    assets_path = 'src/data/asset_details.csv'
    test_path = 'src/data/supplemental_train.csv'
    df_train = load_train_data(train_path)
    df_assets = load_assets(assets_path)
    # visualization
    plot_autocorrelation(df_train, df_assets)
    # train
    models = train(df_train[-100000:], df_assets)
    test_df = load_test_data(test_path)
    x, y = predict_test_data(test_df[0:20000], models)
    x = np.array(x)
    y = np.array(y)
    rho = compute_pearson_corrcoef(x, y)
    print(rho)
