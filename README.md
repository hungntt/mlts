# Cryptocurrencies Prices Prediction

## Motivation

Over the past few years, the use of cryptocurrencies has increased rapidly thanks to the advent of blockchain
technology. Therefore, cryptocurrency price prediction remains an open and extremely challenging forecasting demand,
attracting massive media and investor interest. Approached methods vary from machine learning (ML) methods to deep
learning (DL) related ones. In this project, we will apply three different methods: Light Gradient Boosting Machine,
Support Vector Regression, and Autoregressive Integrated Moving Average to a much bigger time-series dataset from the
Kaggle's G-Research Crypto Forecasting competition. Our methods achieved the top-100 the highest score, where the best
submission scored 0.5405 on the competition's public ranking. The results show that our proposed traditional methods
applied with data-centric approaches can achieve high results when we feed large and high-quality data for the model.

## Installation

Install dependencies:
```
pip install src
```

``` 
pip install -r requirements.txt
```

## Run

### Run methods via Kaggle notebook

- Upload the notebooks in /notebooks to Kaggle notebook.
- Run all cells.

### Run methods on local machines

- Move to models folder:

```
cd /src/models
```

- Run the main of each method. Since the dataset is quite large, the training process may require a long time.

## Dataset

The dataset is stored at ```/src/data/```

## Template

Template repository for data science projects based on https://drivendata.github.io/cookiecutter-data-science/

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`.
    ├── README.md          <- The top-level README for developers using this project.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details,
    │
    ├── .devcontainer      <- Dockerfile with commands
    │   ├── devcontainer.json
    │   └── Dockerfile
    │
    ├── notebooks          <- Jupyter notebooks generating from Kaggle notebook.
    │   ├── arima_train.ipynb
    │   ├── arima_train_local.ipynb
    │   ├── arima-train-local-latest.ipynb
    │   ├── eda.ipynb                 <- Exploration data analysis.
    │   ├── exploited_data.ipynb      <- Code for exploit the data on public leaderboard.
    │   ├── feature_engineering.ipynb <- Analyze and extract features.
    │   ├── preprocessing.ipynb       <- Preprocessing data.
    │   ├── simple_lgbm.ipynb         <- LGBM version without applying optimizing data methods.
    │   ├── upgraded_lgbm.ipynb       <- LGBM version with applying optimizing data methods.
    │   ├── svr.ipynb 
    │   ├── SVR_cluster.ipynb       
    │   ├── SVR_dummy.ipynb
    │   └── SVR_grid_search.ipynb
    │
    ├── visualization      <- Generated visualizations.
    │   └── visualize.py
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting.
    │
    ├── features           <- Features builder.
    │   └── build_features.py        <- Generated graphics and figures to be used in reporting.
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`.
    │
    └── src                <- Source code for use in this project.
       ├── __init__.py     <- Makes src a Python module.
       │
       ├── data            <- Pre-downloaded dataset from the competition.
       │
       ├── features        <- Scripts to turn raw data into features for modeling.
       │   └── build_features.py
       │
       ├── models          <- Scripts to train models and then use trained models to make predictions.
       │   │                 
       │   ├── arima_model.py
       │   ├── lgbm_model.py
       └── └── svr_model.py

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

## Development

### Contribution

Please have a look at our [CONTRIBUTING.md](CONTRIBUTING.md) on how to contribute.


